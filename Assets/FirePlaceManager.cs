using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePlaceManager : MonoBehaviour
{

    public GameObject[] Logs;
    public int Level = 3;
    public GameObject Light;

    public RoomManager roomManager;

    public void DecreaseLevel()
    {
        if (Level - 1 > 0)
        {
            Logs[Level - 1].SetActive(false);
            Level -= 1;
        }
        else
        {
            Logs[0].SetActive(false);
            Level = 0;
            Light.SetActive(false);
        }
    }
    public void IncreaseLevel()
    {
        if (Level <= 3)
        {
            Logs[Level].SetActive(true);
            Light.SetActive(true);
            Level += 1;
        }
        if (Level == 3)
        {
            roomManager.CompleteLevel(4);
        }
    }


}
