using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLevel3 : MonoBehaviour
{
    public RoomManager roomManager;
    public int NumberOfCoinNeeded = 0;
    private int NumberOfCoin = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Coin"))
        {
            return;
        }

        NumberOfCoin++;

        if (NumberOfCoin ==  NumberOfCoinNeeded)
        {
            roomManager.CompleteLevel(3);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            NumberOfCoin--;
        }
    }
}
