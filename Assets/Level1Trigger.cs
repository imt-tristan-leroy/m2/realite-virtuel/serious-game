using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1Trigger : MonoBehaviour
{
    public GameObject StaticPot;
    public RoomManager roomManager;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "BluePotLevel1")
        {
            StaticPot.SetActive(true);
            Destroy(other.gameObject);  

            roomManager.CompleteLevel(1);
        }
    }
}
