using UnityEngine;

public class FirePlaceTrigger : MonoBehaviour
{
    public FirePlaceManager manager;

    private void OnTriggerEnter(Collider other)
    {
        if (manager.Level < 3 && other.gameObject.CompareTag("Flammable"))
        {
            Destroy(other.gameObject);
            manager.IncreaseLevel();
        }
    }
}
