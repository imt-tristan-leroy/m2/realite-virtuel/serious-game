using UnityEngine;

public class MenuFollowCamera : MonoBehaviour
{
    public float offsetRadius = 0.3f;
    public float distanceToHead = 4;

    public Camera Camera;

    [Range(0, 1)]
    public float smoothFactor = 0.5f;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Camera.transform.rotation;

        var cameraCenter = Camera.transform.position + Camera.transform.forward * distanceToHead;

        var currentPos = transform.position;

        var direction = currentPos - cameraCenter;

        var targetPosition = cameraCenter + direction.normalized * offsetRadius;

        transform.position = Vector3.Lerp(currentPos, targetPosition, smoothFactor);

    }
}
