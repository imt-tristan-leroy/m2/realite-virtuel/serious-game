using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Level
{
    public GameObject trigger;
    public GameObject label;

    public void Display()
    {
        if (trigger != null)
        {
            trigger.SetActive(true);
        }
        label.SetActive(true);
    }

    public void Hide()
    {
        if (trigger != null)
        {
            trigger.SetActive(false);
        }
        label.SetActive(false);
    }
}


public class RoomManager : MonoBehaviour
{
    public FirePlaceManager FirePlace;
    public int Level = 0;

    public Level Level1;
    public Level Level2;
    public Level Level3;
    public Level Level4;

    public GameObject Exit;


    public void CompleteLevel(int level)
    {
        if (Level +1 != level)
        {
            return;
        }

        Level += 1;

        if (Level != 4)
        {
            FirePlace.DecreaseLevel();
        }


        switch (Level)
        {
            case 1:
                Level1.Hide();
                Level2.Display();
                break;
            case 2:
                Level2.Hide();
                Level3.Display();
                break;
            case 3:
                Level3.Hide();
                Level4.Display();
                break;
            case 4:
                Level4.Hide();
                Exit.SetActive(true);
                break;
            default: 
                break;
        }
    }

    private void Awake()
    {
        Level1.Display();
        Level2.Hide();
        Level3.Hide();
        Level4.Hide();
        Exit.SetActive(false);

    }
}
