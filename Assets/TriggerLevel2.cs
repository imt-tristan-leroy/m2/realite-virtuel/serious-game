using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLevel2 : MonoBehaviour
{
    public Color color;
    public RoomManager roomManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name != "Ball")
        {
            return;
        }
        Debug.Log(other.GetComponent<Renderer>().material.color.ToString());
        Debug.Log(color.ToString());
        if (other.GetComponent<Renderer>().material.color != color)
        {
            return;
        }

        roomManager.CompleteLevel(2);
    }

}
